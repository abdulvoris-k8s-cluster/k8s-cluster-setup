# Create the setup-server
resource "hcloud_server" "setup-server" {
  count       = 1
  name        = "setup-server"
  image       = "ubuntu-22.04"
  server_type = "cx11"
  datacenter  = "nbg1-dc3"
  ssh_keys    = ["My SSH KEY"]
  public_net  {
    ipv4_enabled = true
    ipv6_enabled = false
  }

  network {
    network_id = hcloud_network.kubernetes-node-network.id
    ip         = "172.16.0.100"
  }

  depends_on = [
    hcloud_network_subnet.kubernetes-node-subnet
  ]
}

# Create the kubernetes nodes
resource "hcloud_server" "kube-node" {
  count       = 3
  name        = "kube-node-${count.index + 1}"
  image       = "ubuntu-22.04"
  server_type = "cx21"
  datacenter  = "nbg1-dc3"
  ssh_keys    = ["My SSH KEY", "SETUP SERVER SSH KEY"]
  public_net  {
    ipv4_enabled = true
    ipv6_enabled = false
  }

  network {
    network_id = hcloud_network.kubernetes-node-network.id
    ip         = "172.16.0.10${count.index + 1}"
  }

  depends_on = [
    hcloud_network_subnet.kubernetes-node-subnet
  ]
}

# Create the node network
resource "hcloud_network" "kubernetes-node-network" {
  name     = "kubernetes-node-network"
  ip_range = "172.16.0.0/24"
}


# Create the node subnet
resource "hcloud_network_subnet" "kubernetes-node-subnet" {
  type         = "cloud"
  network_id   = hcloud_network.kubernetes-node-network.id
  network_zone = "eu-central"
  ip_range     = "172.16.0.0/24"
}


# Create an ssh-key
resource "hcloud_ssh_key" "default" {
  name       = "My SSH KEY"
  public_key = "${var.MY_SSH_KEY}"
}

# Create the setup-server ssh-key
resource "hcloud_ssh_key" "setup_server" {
  name       = "SETUP SERVER SSH KEY"
  public_key = "${var.SETUP_SERVER_SSH_KEY}"
}
