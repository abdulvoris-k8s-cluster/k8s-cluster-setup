# terraform

#### Terraform nima? 

HashiCorp tomonidan ishlab chiqilgan Terraform kodli dasturiy vosita sifatida ochiq manba infratuzilmasi hisoblanadi. Bu sizning infratuzilmangizni ta'minlash, o'zgartirish va boshqarish uchun izchil yondashuvni ta'minlaydi. Infratuzilma jismoniy serverlardan tortib konteynerlar yoki SaaS mahsulotlarigacha bo'lgan hamma narsani anglatishi mumkin. Terraform ushbu barcha infratuzilma komponentlarini resurslar sifatida ko'rib chiqadi va u ushbu resurslarning istalgan holatiga erishish uchun nima qilishini tavsiflovchi ijro rejasini tuzish orqali ishlaydi.

Terraformning muhim jihati uning provayder ekotizimidir. Terraform Provayderlari Terraform turli xizmatlar uchun API bilan o'zaro ishlashda foydalanadigan plaginlardir. Bu Terraform-ga ommaviy bulut provayderlari, bulutli avtomatlashtirish tizimlari, konteynerlarni boshqarish platformalari, uzluksiz integratsiya/yetkazib berish (CI/CD) vositalari, monitoring tizimlari, tarmoq vositalari va boshqalarni oʻz ichiga olgan keng spektrli resurslarni boshqarish imkonini beradi.