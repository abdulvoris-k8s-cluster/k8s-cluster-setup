output "server_local_ips" {
  value = {
    "kube-node-1-local-ip"    = [for node in hcloud_server.kube-node[0].network: node.ip]
    "kube-node-2-local-ip"    = [for node in hcloud_server.kube-node[1].network: node.ip]
    "kube-node-3-local-ip"    = [for node in hcloud_server.kube-node[2].network: node.ip]
    "setup-server-1-local-ip" = [for node in hcloud_server.setup-server[0].network: node.ip]
  }
}

output "server_public_ips" {
  value = {
    "kube-node-1-public-ip"    = hcloud_server.kube-node[0].ipv4_address
    "kube-node-2-public-ip"    = hcloud_server.kube-node[1].ipv4_address 
    "kube-node-3-public-ip"    = hcloud_server.kube-node[2].ipv4_address
    "setup-server-1-public-ip" = hcloud_server.setup-server[0].ipv4_address
  }
}
