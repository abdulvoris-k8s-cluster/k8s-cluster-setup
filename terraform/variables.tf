variable "HCLOUD_API_TOKEN" {
  type = string
  sensitive = true
}

variable "MY_SSH_KEY" {
  type = string
  sensitive = true
}

variable "SETUP_SERVER_SSH_KEY" {
  type = string
  sensitive = true
}